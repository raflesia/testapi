module.exports = (req, res, next) => {
  // Avoid cors errors
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Headers", "*");

  // Automaticly set the date for POST objects
  // if (req.method === "POST") {
  //   res.body.createdAt = Date.now();
  // }
  next();
};
